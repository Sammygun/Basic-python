 _VSCode Coding Tricks - Windows_

1 начало строки home 
2 конец строки end     
3 выдели текст + зажми alt  и переноси стрелками 
4 выдели строку shift + alt = стрелакми вниз ты будешь создавать копии 
5 сразу несколько строк закоментить  выдели + control + /  #  коментишь 
6 multiply  автоподбор сработал 3 символа слова введи или начальное плюс конечное 
    2 или 3 символа переменной 


####################################################################################
_VSCode Coding Tricks - Mac_
1 fn и стрелкой в конец начало 
2 fn + стрелкой вверх вниз 
3 выдели текст = alt(option) = стрелкой вниз вверх 
4 command + /  # коментим строки и разкоментим 
5 multiply        # автоподбор my mty mpt
#################################################################################

1 vs code установка  и настройка
уроки 6-8
2 расширения:
 python (издатель microsoft)
 linter 
в правом нижнем углу vs code выберите интерпретатор
python 3.8 выбери 
справо появиться уведомление установите linter pylint позволяет видеть ошибки до запуска кода
ctrl + shift + m ### позволяет увидеть есть ли проблемы панель problems снизу view = problems

3  ctrl + ` ### вызов терминал в vscode также переключение между текстом и терминалом
 ctrk + b #боковая панель убрать поставить
 ctrl + tab ## переключение 

4 настройка шрифта = справо внизу шарнир = setings = text editor = font size 21 
настройки шрифта терминала = справо внизу шарнир = setings = features = terminal = font size 18 

5 проблемы с синтаксисом
shift + cmd + M   # Проблемы смотрим здесь (view = problems )
shift + ctrl + M  # аналог команды windows 

shift + cmd + p   #  view command palette командная строка 
shift + ctrl + p   # аналог на windows 
= linter = select linter = выбире pylint  самый популярный !!

6 Автоформатирование по pep 8 
shift + cmd + p = format document = справа от тебя предложит установить autopep8 = yes install 
 к примеру x=1   это не по pep8
 shift + cmd + p = format document  # автомотаом после установки все настроит 

6.1 format document делаем автоматом 
code = preferences = в посике галочку поставь = formatOnSave галочку поставь 

7 расширения запуска кода 
extension = code runner = Jun Han = install 
ctrl + alt + n   #  запуск кода 


code = preferences = setting.json
"python": "python -u",   поменял на python 
"python": "python3"



								_Functions_
								_Defining Functions_
								_Types of Functions_
								_Keyword Arguments_
								_Default Arguments_
								_xargs_
								_xxargs_
								_Scope_
								_Debugging_
								 _VSCode Coding Tricks - Windows_







Functions
_Defining Functions_

1 
def greet():              # сама функция 
    print("Hi there")
    print("Welcome aboard")


greet()                 # вызов функции 
Hi there
Welcome aboard


2 параметры функции это переменные которые мы задаем 
аргументы это сами значения которые мы даем самой функции


def greet(first_name, last_name):          #параметры 
    print(f"Hi {first_name} {last_name}")
    print("Welcome aboard")


greet("Sam", "Rybkin")                   # аргументы 
# Hi Sam Rybkin
# Welcome aboard

############################################################################
_Types of Functions_
Два типа функций одна что-то печатает вторая что-то возвращает

1 пример
def greet(first_name, last_name):
    print(f"Hi {first_name} {last_name}")         # просто печатает что-то 


def get_greeting(name):
    return f"Hi {name}"                        # Функция возвращает 


message = get_greeting("Mosh")
print(message)                        переменная message вернет нам нужное значение 
file = open("context.txt", "w")         # открой файл с таким названием, сохрани его 
file.write = (message)                  # запиши туда значение перемнной 


2 пример 
def greet(name):
    print(f"Hi {name}")


# greet("Sam")
print(greet("Sam"))            # вернет значение Sam а потом уже и None
#Hi Sam
#None

####################################################################################################
_Keyword Arguments_

1 
def increment(number, by):
    return number + by


result = increment(2, 1)
print(result)           # можно так но так много кода 
# 3

print(increment(2, 1))  # лучше так python в начале обратиться к increment функции, а потом, 
#создаст невидимую переменную и передаст ее print 
# 3 


2 Второй пример 

def increment(number, by):
    return number + by

print(increment(2, by=1))         # by=1 keyword aregumet так более понятней 
####################################################################################################
_Default Arguments_

1 первый пример 
def increment(number, by=1):       # default argument by должен быть всегда в конце 
    return number + by

print(increment(2))  
# 3 выовод такой так как 1 + 2 = 3 

print(increment(2, 5))
# будет вывод 7 так как by=1 автоматом заменит 5 (2+5)

2 второй пример 
def increment(number, another, by=1):       # default argument by должен быть всегда в конце 
    return number + by

print(increment(2, 5 ))  

3 третий пример 
def increment(number, another, by=1):       # default argument by должен быть всегда в конце
    return number + another + by


print(increment(2, 5))
#############################################################################################
_xargs_

1 
def multiply(x, y):          # данная функция можете передавать ток два значения 
    return x * y


multiply(2, 3)


2 
def multiply(*numbers):
    print(numbers)


multiply(2, 3, 4, 5)
# (2, 3, 4, 5)         # вывод такой будет 


3 третий пример смотри 

def multiply(*numbers):
    for number in numbers:
        print(number)


multiply(2, 3, 4, 5)
# Вывод 
2
3
4
5
#################################################################

4 пример 
def multiply(*numbers):   # сразу несколько значени могу вносить 
    total = 1
    for number in numbers:
        total *= number    # аналог total = total * number
    return total           # обрати внимание на местоположение return 


print(multiply(2, 3, 4, 5)) 
# 120         # будет размер
1 * 2 = 2
2 * 3 = 6 
6 * 4 = 24
24 * 5 = 120

####################################################################
_xxargs_

1 
def save_user(**user):
    print(user)


save_user(id=1, name="John", age=22)
#{'id': 1, 'name': 'John', 'age': 22}


2 
def save_user(**user):
    print(user["name"])  # словарь обращаюсь к конкертному значению 


save_user(id=1, name="John", age=22)
# John                
###################################################################################
_Scope_

1 
def greet():
    message = "a"               


print(message)                        # не сработает разные области видимости 


2 
message = "a"       # глобальная переменнная их надо реже использовать


def greet(name):
    message = "a"


def send_mail(name):
    message = "b"          # разные переменные так как находятся
    # в разных функция


greet("Mosh")



3 

message = "a"       # глобальная переменнная их надо реже использовать


def greet(name):
    message = "b"


greet("Mosh")
print(message)
# a


4 
def greet(name):
    global message     # bad practice это переменная станет
    message = "b"      # как глобальная будет выходить за рамки функции 
#####################################################################################
_Debugging_


1 Рабочий код 
def multiply(*numbers):
    total = 1
    for number in numbers:
        total *= number
    return total


print("Start")
print(multiply(1, 2, 3))


2 НЕрабочий 
def multiply(*numbers):
    total = 1
    for number in numbers:
        total *= number
    	return total       # причина тут 


print("Start")
print(multiply(1, 2, 3))

3 На жучка слева от себя нажимай = Run and debug нажимай = тут можно выбрать и flask django,
 выбирай current python file 


 print("Start")     # тут курсор поставь и нажими f9 или мак fn + f9  =потом
print(multiply(1, 2, 3))        # f10 


shift + f5  # остановка debug 
####################################################################################
 _VSCode Coding Tricks - Windows_

1 начало строки home 
2 конец строки end     
3 выдели текст + зажми alt  и переноси стрелками 
4 выдели строку shift + alt = стрелакми вниз ты будешь создавать копии 
5 сразу несколько строк закоментить  выдели + control + /  #  коментишь 
6 multiply  автоподбор сработал 3 символа слова введи или начальное плюс конечное 
    2 или 3 символа переменной 


####################################################################################
_VSCode Coding Tricks - Mac_
1 fn и стрелкой в конец начало 
2 fn + стрелкой вверх вниз 
3 выдели текст = alt(option) = стрелкой вниз вверх 
4 command + /  # коментим строки и разкоментим 
5 multiply        # автоподбор my mty mpt




