name = 'Alex'
age = 29
family = ['Anna', 'Victor', 'Alex', 'John']

print(f'Тип name: {type(name)}')
print(f'Тип age: {type(age)}')
print(f'Тип family: {type(family)}')

# print(f'Тип name: {type(name)}')
# print(f'Тип age: {type(age)}')
# print(f'Тип family: {type(family)}')
#
#Тип name: <class 'str'>
#Тип age: <class 'int'>
#Тип family: <class 'list'>


#print("#" * 20)
# id place of variable
#print(f'ID name: {id(name)}')
#print(f'ID age: {id(age)}')
#print(f'ID family: {id(family)}')

# underscore_notation = 999
# camelCase = 'Hello World'
#
# # good practice
# my_name = 'Pavel'
# auntName = 'Anna'

# bad practice
# 2pac = 'Old School'
# $money = 'power'
# _age = 22

#import keyword
#kw_list = keyword.kwlist
#print(kw_list)

#for kw in kw_list:         # Выведит с
#       print(kw)

# return = 222            # будет error

# x = 14
# X = 'Python'
# print(x)
# print(X)

# # good
# name
# # bad
# n
#
# # good
# user_address
# # bad
# personUserHomeAddress

